import * as d3 from 'd3';
import "@babel/polyfill";
import * as cst from './consts.js';
import {Tree} from './tree.js';

function* range(start, end) {
  for(let i=start; i<end; i++){
    yield i;
  }
}

const randomBetweenOneAnd = n => Math.floor(Math.random() * (n - 1)) + 1;

const allNodes = [...range(1, 500)].map(n => ({ id: n, parent: randomBetweenOneAnd(n) }));

const svg = d3.select('body')
      .append('svg')
      .attr('width', cst.width + 2*cst.xOffset)
      .attr('height', cst.height + 2*cst.yOffset);

// attaching click handlers to nodes means that
// we should render nodes last, so our clicks don't
// actually land on a link instead.
// grouping the links and nodes together is one way to
// guarantee that we achieve this
const gLinks = svg.append('g').classed('links', true);
const gNodes = svg.append('g').classed('nodes', true);

const t = new Tree(allNodes, svg, gLinks, gNodes, 10);
t.render();
