import * as d3 from 'd3';
import * as cst from './consts.js';

const moveLink = link => link.attr('x1', d => d.source.x + cst.xOffset)
                             .attr('x2', d => d.target.x + cst.xOffset)
                             .attr('y1', d => d.source.y + cst.yOffset)
                             .attr('y2', d => d.target.y + cst.yOffset);

const moveNode = node => node.attr('cx', d => d.x + cst.xOffset)
                             .attr('cy', d => d.y + cst.yOffset)
                             .attr('r', cst.yOffset);

export class Tree {

  constructor(allNodes, svg, gLinks, gNodes, startId) {
    this._allNodes = allNodes;
    this._svg = svg;
    this._gLinks = gLinks;
    this._gNodes = gNodes;

    const _ = this.calculateNodeAndLinkData(startId);
  }

  render() {
    const nodeData = this.nodeData;
    const linkData = this.linkData;

    const nodes = this.nodes.data(nodeData, d => d.id);
    const links = this.links.data(linkData, d => `${d.source.id},${d.target.id}`);

    const newLinks = links.enter()
          .append('line')
          .classed('link', true)
          .attr('x1', d => d.source.x + cst.xOffset)
          .attr('x2', d => d.source.x + cst.xOffset)
          .attr('y1', d => d.source.y + cst.yOffset)
          .attr('y2', d => d.source.y + cst.yOffset)
          .style('stroke', 'black');

    const newNodes = nodes.enter()
          .append('circle')
          .classed('node', true)
          .attr('cx', d => (d.parent == null ? d.x : d.parent.x) + cst.xOffset)
          .attr('cy', d => (d.parent == null ? d.y : d.parent.y) + cst.yOffset)
          .attr('r', 0);

    const oldNodes = nodes.exit().style('opacity', 1);
    const oldLinks = links.exit().style('opacity', 1);

    const selections = [
      [oldNodes, oldLinks, nodes, links], // old + existing

      [newNodes.filter(x => x.depth === 0)], // new nodes and links
      [newNodes.filter(x => x.depth === 1), newLinks.filter(x => x.target.depth === 1)],
      [newNodes.filter(x => x.depth === 2), newLinks.filter(x => x.target.depth === 2)],
      [newNodes.filter(x => x.depth === 3), newLinks.filter(x => x.target.depth === 3)],
    ];

    const transitions = [
      // remove old + update existing
      [x => x.style('opacity', 0).remove(),
       x => x.style('opacity', 0).remove(),
       x => moveNode(x.attr('transform', 'translate(0,0)')),
       x => moveLink(x.attr('transform', 'translate(0,0)'))],

      // add new
      [moveNode], // depth 0

      [moveNode,  // depth 1
       moveLink],

      [moveNode,  // depth 2
       moveLink],

      [moveNode,  // depth 3
       moveLink],
    ];

    doTransitions(selections,
                  transitions,
                  cst.transitionTime,
                  () => this.bindNodeClickHandlers());
  }

  translateAll(newRoot, c) {
    const translateX = newRoot.x - c.x;
    const translateY = newRoot.y - c.y;

    applyTransition(this.all,
                     x => x.attr('transform', `translate(${translateX},${translateY})`),
                     cst.transitionTime,
                     () => this.render());
  }

  bindNodeClickHandlers() {

    this.nodes.on('click', c => {
      this.nodes.on('click', null);
      const newRoot = this.calculateNodeAndLinkData(c.id);
      this.translateAll(newRoot, c);
    });

  }

  calculateNodeAndLinkData(id) {
    const newRoot = mkTree(getNodesToDisplay(this._allNodes, id));
    const newDescendants = newRoot.descendants();
    const newLinks = newRoot.links();

    this.nodeData = newDescendants;
    this.linkData = newLinks;
    return newRoot;
  }

  get all() {
    return this._svg.selectAll('circle.node,line.link');
  }

  get nodes() {
    return this._gNodes.selectAll('circle.node');
  }

  get links() {
    return this._gLinks.selectAll('line.link');
  }

}


// applies a transform to everything in a selection
// then calls callback when finished
const applyTransition = (selection, transition, timePerTransition, cb) => {
  const ease = d3.transition().duration(timePerTransition).ease(d3.easeLinear);

  let ctr = 0;
  transition(selection.transition(ease))
    .on('start', () => ++ctr)
    .on('end', () => {
      if (--ctr === 0){
        cb();
      }
    });
};

// example use: doTransitions([[a, b], [c]], [[t_a, t_b], [t_c]], t, () => console.log('done'));
// applies transition t_a to selection a, and t_b to selection b simultaneously (transition takes time t)
// when that has finished applies t_c to selection c (transition takes time t)
// when that has finished logs 'done'
const doTransitions = (selections, transitions, timePerTransition, cb) => {

  // delay is currently implemented using numbers of milliseconds
  // if this is not precise enough could implement using events instead
  // but it is fine for now

  if (selections.length !== transitions.length) {
    throw new Error('Every selections array should have exactly one transitions array');
  }

  const ease = d3.transition().duration(timePerTransition).ease(d3.easeLinear);

  let delay = 0;

  for (let i=0; i<selections.length; i++) {
    const sls = selections[i];
    const ts = transitions[i];

    if (sls.length !== ts.length) {
      throw new Error('Every selection needs to have a transition');
    }

    if (sls.every(s => s.empty())) {
      continue;
    }

    for(let j=0; j<sls.length; j++) {
      const s = sls[j];
      const t = ts[j];

      const delayedSelection = s.transition(ease).delay(delay);
      t(delayedSelection);
    }

    delay += timePerTransition;
  }

  setTimeout(() => cb(), delay);
};

// calculates x,y coords of nodes and links to visualise a set of data
const mkTree = rawNodes => {
  const tree = d3.tree();
  tree.size([cst.width, cst.height]);
  const root = getRoot(rawNodes);
  tree(root);
  return root;
};

// gets 3 generations from a set of data
const getNodesToDisplay = (allNodes, rootId) => {
  let layer1 = allNodes.filter(x => x.parent == rootId);
  let layer2 = allNodes.filter(x => layer1.some(y => y.id == x.parent));
  let layer3 = allNodes.filter(x => layer2.some(y => y.id == x.parent));
  let rootNode = allNodes.find(x => x.id == rootId);
  return [{ id: rootId }].concat(layer1).concat(layer2).concat(layer3);
};

const getRoot = nodes => d3.stratify()
      .id(d => d.id)
      .parentId(d => d.parent)(nodes);
